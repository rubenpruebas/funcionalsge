## 1º Definir las necesidades detectadas en tu proyecto empresarial.
**Sistema de citas:** Un sistema que permita a los usuarios ver y reservar citas disponibles. Las citas disponibles actualmente son el martes a las 16:00 y el jueves a las 11:30. Se podría implementar una función que muestre las citas no reservadas (el “negativo” de las reservadas).

**Notificaciones de vacunación:** En marzo, hay un subsidio gubernamental para la vacunación contra la rabia. El sistema debería ser capaz de notificar a todos los clientes que no hayan vacunado a su perro en los últimos 9 meses. Además, debería enviar una notificación cuando haya pasado un año desde la última vacunación.

**Informes mensuales:** El sistema debería ser capaz de generar informes sobre las consultas y vacunaciones realizadas cada mes.


<br>

<br>

## 2º Definir la solución tecnológica a implementar.
Se ha decidido utilizar Zoho, una plataforma en la nube con diversas herramientas empresariales. Zoho permite gestionar datos, automatizar procesos y analizar información, ofreciendo así una solución completa y eficaz para el proyecto.

<br>

<br>


## 3º Enumerar las ventajas de implementar tecnológicamente la solución propuesta
Zoho es adecuado para el tamaño de la empresa con la que trabajamos. Permite tener todo lo que vamos a implementar centralizado en sus soluciones.

<br>

<br>

## 4º Definición de un funcional de la solución a implementar. 


### Funcionalidades:
- Sistema que te devuelva las citas disponibles. _Citas disponibles: Martes 16:00 Jueves 11:30_ \
   idea: hacer el negativo de las reservadas
- En el mes de marzo hay un subsidio gubernamental para vacunar a los perros de rabia,\
   avisar a todos los clientes que no hayan vacunado a su perro en los ultimos 9 meses
- Paralelamente a la notificación anterior se notifica cuando ha pasado un año desde la última vacunación.
- Informe sobre las consultas realizadas este mes
- Informe sobre las vacunaciones realizadas este mes

### Veterinarios:
- Nombre
- DNI
- Número colegiado
- Email
- Teléfono

### Clientes (dueños):
- Nombre completo
- DNI
- Teléfono
- Correo
- Dirección

### Mascotas:
- Nombre
- Número chip
- Dni dueño
- Perro/gato
- Fecha última vacuna rabia

### Consultas: 	
- Número chip mascota
- Dni dueño perro
- Fecha (puede haberse realizado o ser una futura consulta reservada)	
- Tipo consultas: vacunas, revisión, otro, operacion, pequeña intervención, 
- Veterinario/s que han participado		
- Descripción de los síntomas, tratamiento realizado en consulta y medicación prescrita. 
- Precio
